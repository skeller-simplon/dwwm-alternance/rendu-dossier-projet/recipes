import Vue from "vue";
import { Store } from "vuex"
import { IziToast } from "../plugins/iziToast"
import User from '../transport/User';

export default class VueWithMixins extends Vue{
    super
    Toast: IziToast;
    $store: Store<any>
    connectedUser: User
    axios
}