import axios from "axios";
import toast from './plugins/iziToast';
import store from "../src/store/index"

axios.interceptors.request.use(function(config) { 
    if( localStorage.getItem('token')){
        config.headers = {
            Authorization: 'Bearer '+localStorage.getItem('token'),
        };
    }
    return config;
});

axios.interceptors.response.use(function(response) {
    return response;
}, function (error) {
    console.log(error)
    console.log(error.response.data)
    if (error.response.data.name === "FunctionalError") {
        toast.error(error.response.data.message);
    }
    if (error.response.data.name === "TokenExpiredError") {
        toast.error(error.response.data.message);
        localStorage.removeItem("token")
        // store.dispatch("showLoginModal")
    }
})