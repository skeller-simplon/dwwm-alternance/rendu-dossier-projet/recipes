import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import './assets/RecipeStyle.less'
import "./auth-interceptor";
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { required } from 'vee-validate/dist/rules';
import { GlobalMixin } from './mixins/GlobalMixins';
import store from './store';
import iziToast from "./plugins/iziToast"
import axios from "axios";
import VueHtmlToPaper from 'vue-html-to-paper';

const htmlToPaperOptions = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://cdn.jsdelivr.net/npm/@mdi/font@5.x/css/materialdesignicons.min.css',
    'https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css',
    "@/assets/RecipeStyle.less"
  ]
}
Vue.use(VueHtmlToPaper, htmlToPaperOptions);

extend('required', {
  ...required,
  message: 'Ce champ est obligatoire'
});

Vue.config.productionTip = false;

Vue.mixin(GlobalMixin)
Vue.component("ValidationProvider", ValidationProvider)
Vue.component("ValidationObserver", ValidationObserver);


Vue.filter('toCamelCase', str => {
  return str
      .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) { return $1.toLowerCase(); });
})
Vue.prototype.Toast = iziToast;
Vue.prototype.$axios = axios;
Vue.use({
  store,
  install (Vue, options) {
    Vue.prototype.$myStore = store
    // Vue.prototype.connectedUser = store.state.connectedUser
  }
})

new Vue({
  mixins: [GlobalMixin],
  router,
  store,
  vuetify,
  
  render: (h) => h(App)
}).$mount('#app');
