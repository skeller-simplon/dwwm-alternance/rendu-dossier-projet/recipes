import Vue from "vue";
import iziToast from "../plugins/iziToast"
import moment from "moment";
    
moment.locale("fr")

export function timeInputToMilliSeconds(time: string): number
{
    let timeArr = time.split(":")
    let r = parseInt(timeArr[0]) * 60 + parseInt(timeArr[1])
    return r * 60 * 1000
}

export function isVoyel(str: string){
    if(str.length > 1)
        return null
    str = str.toUpperCase();
    return str == "A" || str == "E" || str == "I" || str == "O" || str == "U";
}

/**
 * Transforme des millisecondes en un texte affichable
 * @param millSeconds milliseconds
 */
export function milliSecondsToDisplayTime(millSeconds: number){
    var tempTime = moment.duration(millSeconds);
    let endString = "";
    endString += tempTime.hours() ? tempTime.hours() + "h" : "";
    endString += tempTime.minutes() ? tempTime.minutes() + "m" : "";
    return endString;
}

export const toDisplayDateTime = (str: Date) => {
    return moment(str).format('DD MMMM YYYY, h:mm');
}

Vue.filter("toDisplayDateTime", toDisplayDateTime)
Vue.filter("milliSecondsToDisplayTime", milliSecondsToDisplayTime)
        
export class GlobalMixin extends Vue{
    Toast = iziToast;
}