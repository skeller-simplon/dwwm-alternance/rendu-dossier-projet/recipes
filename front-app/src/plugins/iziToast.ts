import 'izitoast/dist/css/iziToast.min.css'
import iZtoast from 'izitoast'

export interface IziToast{
    error: (message: string, title?: string) => void
    success: (message: string, title?: string) => void
    warning: (message: string, title?: string) => void
}
const toast: IziToast = {
    error: (message, title = 'Erreur') => {
        return iZtoast.error({
            title: title,
            message: message,
            position: 'bottomCenter'
        });
    },
    success: (message, title = 'Succès') => {
        return iZtoast.success({
            title: title,
            message: message,
            position: 'bottomCenter'
        });
    },
    warning: (message, title = 'Attention') => {
        return iZtoast.warning({
            title: title,
            message: message,
            position: 'bottomCenter'
        });
    }
};

export default toast;