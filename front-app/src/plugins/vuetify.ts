import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/es5/util/colors';
// Translation provided by Vuetify (typescript)
import fr from 'vuetify/src/locale/fr'

const customTheme = {
    primary: colors.green.base,
    secondary: colors.amber.base,
    accent: colors.purple.base,
    error: colors.red.base,
    warning: colors.yellow.base,
    info: colors.teal.base,
    success: colors.lime.base
}
Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: { fr },
        current: "fr"
    },
    theme: {
        themes: {
            light: customTheme
        }
    }
})
