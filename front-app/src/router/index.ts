import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';

import LoginPage from "../views/global/LoginPage.vue";
import DisplayRecipe from '../views/recipe/DisplayRecipe.vue'
import RecipeForm from '../views/recipe/RecipeForm.vue'
import UserProfile from '../views/user/UserProfile.vue'

import LoginModal from "../views/global/LoginModal.vue"

Vue.component("app-login-modal", LoginModal);
Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/login',
    name: "login",
    component: LoginPage
  },
  {
    path: '/recipe/add',
    name: "recipe-add",
    component: RecipeForm
  },
  {
    path: '/recipe/:id',
    name: "recipe",
    props: true,
    component: DisplayRecipe
  },
  {
    path: '/user/:userId',
    name: "user-profile",
    props: true,
    component: UserProfile,
  },
  { path: '*', redirect: {name: 'home'} }
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
