import axios from "axios";
import BaseService from "./BaseService";
import store from '@/store';
import User from '@/transport/User';

export default class AuthService extends BaseService {
    private authApiPath = this.apiPath + "auth"
    public login(email: string, password: string) {
        let body = {
            email: email,
            password: password
        }
        
        return axios.post(this.authApiPath + "/login", body).then(res => {
            if (res.status === 200 && res.data.token) {
                localStorage.setItem("token", res.data.token)
                this.getConnectedUser().then(res => {
                    store.dispatch('login', res);
                })
            }
        })
    }
    public logout(){
        store.dispatch('logout');
    }
    public getConnectedUser(){
        return axios.get(this.authApiPath + "/getConnectedUser").then(res => {
            if(res) {
                store.dispatch('login', res.data)
                return Object.assign(new User(), res.data) ;
            }
        })
    }
}