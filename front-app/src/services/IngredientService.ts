import axios from "axios";
import BaseService from "./BaseService";
import Ingredient from '@/transport/Ingredient';

export default class IngredientService extends BaseService{
    private ingredientApiPath = this.apiPath + "ingredients/"

    public getIngredientById(id: number): Promise<Ingredient>{
        return axios.get(this.ingredientApiPath + id).then(async res => res.data)
    }
    public searchIngredients(searchString: string = "", count: number = 10): Promise<Ingredient[]>{
        const body = {
            searchString: searchString,
            count: count
        }
        return axios.post(this.ingredientApiPath + "searchIngredients", body).then(res => res.data)
    }
    public createIngredient(label: string){
        const body = {
            Label: label
        }
        return axios.post(this.ingredientApiPath, body).then(res => res.data)
    }
}