import axios from "axios";
import BaseService from "./BaseService";
import RecipeIngredientType from "../transport/RecipeIngredientType";

export default class RecipeIngredientService extends BaseService{
    private recipeIngredientApiPath = this.apiPath + "recipeIngredients";

    public getRecipeIngredientTypes(){
        return axios.get(this.recipeIngredientApiPath + "/types").then(res => {
            return res.data as RecipeIngredientType[]; 
        })
    }
}