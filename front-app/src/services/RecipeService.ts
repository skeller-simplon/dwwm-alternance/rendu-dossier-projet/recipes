import axios from "axios";
import BaseService from "./BaseService";
import Recipe from "../transport/Recipe";
import RecipeIngredient from '@/transport/RecipeIngredient';
import RecipeStep from '@/transport/RecipeStep';
import User from '@/transport/User';

export default class RecipeService extends BaseService{
    private recipeApiPath = this.apiPath + "recipes"

    public getAll(){
        return axios.get(this.recipeApiPath)
    }
    public searchRecipes(searchString: string){
        const body = {
            searchString: searchString
        }
        return axios.post(this.recipeApiPath + "/searchRecipes", body).then(res => {
            let ret: Recipe[] = []
            if(res.data.length == 0) return []
            res.data.map(user => ret.push(Object.assign(new Recipe(), user)))
            return ret
        })
    }
    /**
     * Retourne les dernières recettes entrées.
     * @param limit Nombre d'entrées retournées 
     */
    public getLastRecipes(limit: number = 10): Promise<Recipe[]>{
        let body = {limit: limit}
        return axios.get(this.recipeApiPath + "/lastRecipes", {params: body}).then(res => res.data as Recipe[])
    }
    
    /**
     * Récupère toutes les recettes d'un utilisateur
     * @param userId Id de l'utilisateur
     */
    public getRecipesForUser(userId: string): Promise<Recipe[]>{
        return axios.get(this.recipeApiPath + "/byUserId/" + userId)
        .then(res => res.data as Recipe[])
    }

    /***
     * Récupères une recette par son Id
     * @param recipeId Id de la recette
     */
    public getOneRecipeById(recipeId: number): Promise<Recipe>{
        return axios.get(this.recipeApiPath + "/" + recipeId).then(res => {
            let recipe = new Recipe(res.data.Id, res.data.Title, res.data.Comment, res.data.Duration, Object.assign(new User(), res.data.User), [], []);
            if(!recipe) return null;
            res.data.RecipeIngredients.map(recipeIngredient => {
                recipe.RecipeIngredients.push(Object.assign(new RecipeIngredient(), recipeIngredient))
            });
            res.data.RecipeSteps.map(recipeStep => {
                recipe.RecipeSteps.push(Object.assign(new RecipeStep(), recipeStep))
            });
            return recipe
        })
    }

    /**
     * Créer une recette
     * @param recipe 
     */
    public createRecipeWithFullDetails(recipe: Recipe): Promise<Recipe>{
        let body = {
            recipe : {Title: recipe.Title, Comment: recipe.Comment, Duration: recipe.Duration},
            steps: recipe.RecipeSteps,
            ingredients: recipe.RecipeIngredients
        }

        return axios.post(this.recipeApiPath + "/createWithFullDetail" , body).then(res => res.data as Recipe)
    }
}