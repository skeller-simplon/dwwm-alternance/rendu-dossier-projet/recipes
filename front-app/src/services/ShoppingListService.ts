import axios from "axios";
import BaseService from "./BaseService";
import ShoppingList from "../transport/ShoppingList"
import RecipeIngredient from '@/transport/RecipeIngredient';

export default class ShoppingListService extends BaseService{
    private shoppingListApiPath = this.apiPath + "shoppingList"

    public getShoppingListFromUser(userId: number){
        let param = {userId: userId}
        return axios.get(this.shoppingListApiPath + "/getShoppingListFromUser", {params: param}).then(res => {
            if(!res.data) return null
            let objList = new ShoppingList(res.data.Id, res.data.userId, []);
            res.data.RecipeIngredients.map(recipeIngredient => {
                objList.RecipeIngredients.push(Object.assign(new RecipeIngredient(), recipeIngredient))
            });
            return objList
        })
    }

    public emptyUserList(userId: number){
        let param = {userId: userId}
        return axios.post(this.shoppingListApiPath + "/deleteListForUser", param).then(res => res.data)
    }

    public addRecipeToShoppingList(userId: number, recipeId: number){
        let params = {
            userId: userId,
            recipeId: recipeId
        }
        return axios.post(this.shoppingListApiPath + "/addRecipeToShoppingList", params).then(res => res.data)
    }
}