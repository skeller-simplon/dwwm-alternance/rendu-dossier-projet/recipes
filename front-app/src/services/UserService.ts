import axios from "axios";
import BaseService from "./BaseService";

import User from "../transport/User";
export default class UserService extends BaseService {
    private userApiPath = this.apiPath + "users"
    public searchUsers(searchString: string){
        const body = {
            searchString: searchString
        }
        return axios.post(this.userApiPath + "/searchUsers", body).then(res => {
            let ret: User[] = []
            if(res.data.length == 0) return []
            res.data.map(user => ret.push(Object.assign(new User(), user)))
            return ret
        })
    }

    public getUserById(id: string): Promise<User> {
        return axios.get(this.userApiPath + "/" + id).then(res => Object.assign(new User(), res.data))
    }

    public createUser(email: string, firstname: string, lastname: string, birthdate: string, password: string){
        let body = {
            Email: email,
            Password: password,
            Firstname: firstname,
            Lastname: lastname,
            Birthdate: birthdate,
        }
        return axios.post(this.userApiPath, body).then(res => res.data)
    }
}