import Vue from 'vue';
import Vuex from 'vuex';
import User from "@/transport/User";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    connectedUser: null,
    showLoginModal: false
  },
  mutations: {
    CHANGE_USER(state, payload) {
      state.connectedUser = payload;
    },
    CHANGE_SHOW_LOGIN_MODAL(state, payload) {
      state.showLoginModal = payload;
    }
  },
  actions: {
    login({commit}, connectedUser) {
      commit('CHANGE_USER', connectedUser as User);
    },
    logout({commit}) {
      commit('CHANGE_USER', null);
      localStorage.removeItem('token');
    },
    showLoginModal({commit}){ commit('CHANGE_SHOW_LOGIN_MODAL', true); },
    hideLoginModal({commit}){ commit('CHANGE_SHOW_LOGIN_MODAL', false); },
  },
  modules: {
  },
});
