export default class Ingredient{
    public Id: number = null
    public Label: string = ""
    public ImageLink: string = ""
    
    constructor(id: number = null, label: string = "", imageLink?: string){
        this.Id = id;
        this.Label = label;
        this.ImageLink = imageLink;
    }
}