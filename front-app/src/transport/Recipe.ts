import RecipeIngredient from './RecipeIngredient';
import RecipeStep from './RecipeStep';
import User from './User';

export default class Recipe{
    public Id: number;
    public Title: string;
    public Comment: string;
    public Duration: number;
    public RecipeIngredients: RecipeIngredient[]
    public RecipeSteps: RecipeStep[]
    public User: User
    public UserId: number

    constructor(Id: number = null, Title: string = "", Comment: string = "", Duration: number = null, user: User = null, ingredients: RecipeIngredient[] = [], steps: RecipeStep[] = []){
        this.Id = Id;
        this.Title = Title;
        this.Comment = Comment;
        this.Duration = Duration;
        this.User = user;
        this.RecipeIngredients = ingredients;
        this.RecipeSteps = steps;
    }
}