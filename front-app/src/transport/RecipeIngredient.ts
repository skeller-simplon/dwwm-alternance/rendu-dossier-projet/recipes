import Ingredient from "./Ingredient"
import RecipeIngredientType from './RecipeIngredientType';
import { isVoyel } from '@/mixins/GlobalMixins';

export default class RecipeIngredient{
    public Id?: number;
    public Amount?: number;
    public RecipeId?: number;
    public RecipeIngredientType?: RecipeIngredientType;
    public RecipeIngredientTypeId?: number
    public Ingredient?: Ingredient
    public IngredientId?: number
    public ShoppingListId?: number

    constructor(amount?: number, ingredient?: Ingredient, type?: RecipeIngredientType){
        this.Amount = amount;
        this.Ingredient = ingredient;
        this.RecipeIngredientType = type;
    }

    /**
    * Transforme un Recipe_Ingredient en texte affichable
    * 2, null, "oeuf" => 2 oeufs
    * 100, "G", "sucre" => 100G de sucre
    */
    public get ComputedLabel(): string {
        let recipeText = this.Amount.toString();
        // On a un symbole, on l'ajoute avec de ou d'
        if (this.RecipeIngredientType.Symbol){
            recipeText += ' ' + this.RecipeIngredientType.Symbol;
            recipeText += isVoyel(this.Ingredient.Label[0]) ? " d' " : " de ";
        }
        // Pas de symbole, on accorde juste
        recipeText += " " + this.Ingredient.Label
        recipeText += this.Amount > 1 ? "s" : ""
        return recipeText;
    }
}