export default class RecipeIngredientType {
    public Id: number;
    public Key: string;
    public Label?: string;
    public Symbol?: string;
} 