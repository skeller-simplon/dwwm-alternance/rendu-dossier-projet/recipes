export default class RecipeStep{
    public Id?: number = null;
    public Text: string = "";
    public Order?: number = null;

    constructor(text: string = "", order: number = null){
        this.Text = text;
        this.Order = order;
    }
}