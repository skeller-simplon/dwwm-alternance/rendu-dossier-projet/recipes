import User from './User';
import Recipe from './Recipe';

export default class SearchItem{
    public Label: string; 
    public Id: number; 
    public Type: string; 

    constructor(label: string = "", id: number = null, type: string = ""){
        this.Label = label;
        this.Type = type;
        this.Id = id;
    }
}
export const usersToSearchItems = (users: User[]): SearchItem[] => {
    return users.map(user => new SearchItem(user.Fullname, user.Id, "USER"))
}
export const recipesToSearchItems = (recipes: Recipe[]): SearchItem[] => {
    return recipes.map(recipe => new SearchItem(recipe.Title, recipe.Id, "RECIPE"))
}
