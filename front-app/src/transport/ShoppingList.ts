import RecipeIngredient from './RecipeIngredient';

export default class ShoppingList{
    public Id: number; 
    public UserId: number; 
    public RecipeIngredients: RecipeIngredient[]; 

    constructor(id: number = null, userId: number = null, ingredients: RecipeIngredient[] = []){
        this.Id = id;
        this.UserId = userId;
        this.RecipeIngredients = ingredients;
    }
}

