export default class User {
    public Id: number
    public Firstname: string
    public Lastname: string
    public Email: string
    public Birthdate: Date
    public CreationDate: Date
    public LastUpdateDate:Date
    public Password?: string

    public get Fullname(): string{
        return this.Firstname + ' ' + this.Lastname;
    }
}