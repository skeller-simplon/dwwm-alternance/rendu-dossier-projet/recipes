import Vue from "vue";
import { Store } from "vuex"
import { IziToast } from "./plugins/iziToast"
import User from './transport/User';
import axios from "axios";
import VueHtmlToPaper from 'vue-html-to-paper';

declare module 'vue/types/vue' {
    interface Vue {
        Toast: IziToast;
        $store: Store<any>;
        $htmlToPaper: VueHtmlToPaper;
        connectedUser: User;
        axios: typeof axios;
    }
  }