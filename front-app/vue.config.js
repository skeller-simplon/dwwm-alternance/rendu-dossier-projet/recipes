module.exports = {
  transpileDependencies: [
    'vuetify',
  ],
  // Erreur générée si pas activée. L'app ne détecte pas les vue comme étant des modules ts
  chainWebpack: config => {
    config.plugins.delete('fork-ts-checker');
    config.module
      .rule('ts')
      .use('ts-loader')
      .tap(options => { return {...options, 'transpileOnly': false }});
  }
};
