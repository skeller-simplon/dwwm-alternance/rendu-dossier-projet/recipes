import sequelize_fixtures from 'sequelize-fixtures';
import ingredientFixtures from './ingredientFixtures.json'
import chocolatCakeFixtures from './chocolatCakeFixtures.json';
import userFixtures from './userFixtures.json';
import recipeTypesFixtures from './recipeTypesFixtures.json';
import { Sequelize } from 'sequelize/types';

export default class FixtureFactory {
    static run(db: Sequelize) {
        console.log("[Fixtures] Début fixtures")
        return db.models.Ingredient.findOne().then(ingredient => {
            if (!ingredient) {
                console.log("[Fixtures] Création ingredients")
                return sequelize_fixtures.loadFixtures(ingredientFixtures, db.models)
            }
        }).then(() => {
            return db.models.User.findOne().then(user => {
                if (!user) {
                    console.log("[Fixtures] Création users")
                    return sequelize_fixtures.loadFixtures(userFixtures, db.models, {transformFixtureDataFn: user => {
                        user.Birthdate = new Date(1960, 6, 24);
                        return user;
                    }})
                }
            })

        }).then(() => {
            // Pour l'instant, les types et les recipeIngredients ne sont pas reliés par des id
            // mais seulement de manière fonctionne par des labels.
            return db.models.RecipeIngredientType.findOne().then(type => {
                if (!type) {
                    console.log("[Fixtures] Création types d'ingredient")
                    return sequelize_fixtures.loadFixtures(recipeTypesFixtures, db.models)
                }
            })    
        }).then(() => {
            return db.models.Recipe.findOne().then(recipe => {
                if (!recipe) {
                    console.log("[Fixtures] Création gateau au chocolat")
                    return sequelize_fixtures.loadFixtures(chocolatCakeFixtures, db.models)
                }
            })            
        }).finally(() => {
            console.log("[Fixtures] Fin fixtures")
        }).catch(err => {
            console.error(err)
        })
    }
}