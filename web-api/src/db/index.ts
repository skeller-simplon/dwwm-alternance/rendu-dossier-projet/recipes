import { Sequelize, Options, json, Op } from "sequelize";
import dotenv from 'dotenv';
import FixtureFactory from "./fixtures/FixtureFactory";
dotenv.config()

const opts: Options = {
  quoteIdentifiers: true,
  logging: false,
  define: {
    freezeTableName: true,
    updatedAt: 'LastUpdateDate',
    createdAt: 'CreationDate',
  },
  operatorsAliases: {
    $like: Op.like,
    $not: Op.not
  }
}

const sequelize = new Sequelize(`mysql://${process.env.MYSQL_USER}:${process.env.MYSQL_PASSWORD}@127.0.0.1:3306/recipes`, opts);

// Test connexion serveur SQL
sequelize.authenticate()
  .then(() => {
    // DEBUG: Drop tables et les recréé, on peut aussi utiliser sync avec le mot clé force
    // sequelize.drop().then(() => {
      console.log('[mysql] Connection has been established successfully.');
      return sequelize.sync().then(db => {
        return FixtureFactory.run(db)
      })
    // })
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err.message);
  });
  
  export default sequelize;