import Sequelize from "sequelize";
import sequelize from ".."
import RecipeIngredient from "./RecipeIngredient";
import computeImage from "../../utils/unsplash"


interface IngredientInterface{
  Id: number
  Label: string
  ImageLink: string
}

export default class Ingredient extends Sequelize.Model<IngredientInterface> {
  public Id: number;
  public Label: string;
  public ImageLink: string
}

Ingredient.init({
  Id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  Label: {
    type: Sequelize.STRING,
    allowNull: false
  },
  ImageLink: {
    type: Sequelize.STRING,
    allowNull: true
  },
}, {
  modelName: 'Ingredient',
  timestamps: false,
  sequelize: sequelize,
  hooks: {
    beforeCreate : async ingredient => {
      let image: string = await computeImage(ingredient.Label);
      ingredient.set("ImageLink", image);
    }
  }
});


