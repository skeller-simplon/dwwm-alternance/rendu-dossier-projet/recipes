import Sequelize from "sequelize";
import RecipeIngredient from "./RecipeIngredient";
import RecipeStep from "./RecipeStep";
import sequelize from "..";

interface RecipeInterface {
    Id: number;
    Title: string;
    Comment: string;
    Duration: number;
    RecipeIngredients: RecipeIngredient[]
    RecipeSteps: RecipeStep[]
}
export default class Recipe extends Sequelize.Model<RecipeInterface>{
    public Id: number;
    public Title: string;
    public Comment: string;
    public Duration: number;
    public RecipeIngredients: RecipeIngredient[]
    public RecipeSteps: RecipeStep[]
}

Recipe.init({
    Id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    Title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    Comment: {
        type: Sequelize.STRING,
        allowNull: true
    },
    Duration: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
}, {
    modelName: 'Recipe',
    sequelize: sequelize
})

Recipe.hasMany(RecipeIngredient, {
    foreignKey: {
        allowNull: true,
        defaultValue: null
    }
});
RecipeIngredient.belongsTo(Recipe)

Recipe.hasMany(RecipeStep)
RecipeStep.belongsTo(Recipe);