import Sequelize from "sequelize";
import sequelize from ".."
import Ingredient from "./Ingredient";
import Recipe from "./Recipe";
import ShoppingList from "./ShoppingList";
import RecipeIngredientType from "./RecipeIngredientType";

interface RecipeIngredientInterface{
  Id: number;
  Amount: string;
  Type?: RecipeIngredientType;
  Ingredient: Ingredient;
}
export default class RecipeIngredient extends Sequelize.Model<RecipeIngredientInterface> {
  public Id: number;
  public Amount: string;
  public RecipeIngredientType?: RecipeIngredientType;
  public Ingredient: Ingredient;
}

RecipeIngredient.init({
  Id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  Amount: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
}, {
  modelName: 'RecipeIngredient',
  sequelize: sequelize,
  timestamps: false,
});

Ingredient.hasMany(RecipeIngredient)
RecipeIngredient.belongsTo(Ingredient)
