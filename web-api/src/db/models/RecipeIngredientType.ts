import Sequelize from "sequelize";
import sequelize from "../index"
import RecipeIngredient from "./RecipeIngredient";

interface RecipeIngredientTypeInterface{
  Id: number;
  Key: string;
  Label?: string;
  Symbol?: string;
}
export default class RecipeIngredientType extends Sequelize.Model<RecipeIngredientTypeInterface> {
  public Id: number;
  public Key: string;
  public Label?: string;
  public Symbol?: string;
}

RecipeIngredientType.init({
  Id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  Key: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Label: {
    type: Sequelize.STRING,
    allowNull: true
  },
  Symbol: {
    type: Sequelize.STRING,
    allowNull: true
  },
}, {
  modelName: 'RecipeIngredientType',
  sequelize: sequelize,
  timestamps: false
});

RecipeIngredientType.hasMany(RecipeIngredient, {onDelete: "CASCADE"})
RecipeIngredient.belongsTo(RecipeIngredientType)