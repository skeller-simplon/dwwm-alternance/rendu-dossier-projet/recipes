import Sequelize from "sequelize";
import sequelize from "..";

interface RecipeStepInterface {
    Id: number;
    Text: string;
    Order: number;
}
export default class RecipeStep extends Sequelize.Model<RecipeStepInterface>{
    public Id: number;
    public Text: string;
    public Order: number;
}

RecipeStep.init({
    Id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    Text: {
        type: Sequelize.TEXT({length: 'medium'}),
        allowNull: false
    },
    Order: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
    modelName: 'RecipeStep',
    sequelize: sequelize,
    timestamps: false
})