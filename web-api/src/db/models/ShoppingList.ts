import Sequelize from "sequelize";
import sequelize from ".."
import RecipeIngredient from "./RecipeIngredient";

interface ShoppingListInterface {
  Id: number;
  UserId: number
  RecipeIngredients: RecipeIngredient[]; 
}
export default class ShoppingList extends Sequelize.Model<ShoppingListInterface> {
  public Id: number;
  public UserId: number
  public RecipeIngredients: RecipeIngredient[]; 
}

ShoppingList.init({
  Id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
}, {
  modelName: 'ShoppingList',
  timestamps: false,
  sequelize: sequelize
});


ShoppingList.hasMany(RecipeIngredient, {
  foreignKey: {
    allowNull: true,
    defaultValue: null
  }
});


