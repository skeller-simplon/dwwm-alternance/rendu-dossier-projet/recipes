import Sequelize from "sequelize";
import sequelize from ".."
import ShoppingList from './ShoppingList'
import Recipe from './Recipe'
import bcrypt from "bcrypt";
import FunctionalError from "../../transport/FunctionalError";

interface UserInterface{
  Id: number
  Firstname: string
  Lastname: string
  Email: string
  Password: string
  Birthdate: Date
}
export default class User extends Sequelize.Model<UserInterface> {
  public Id: number
  public Firstname: string
  public Lastname: string
  public Email: string
  public Password: string
  public Birthdate: Date
}

User.init({
  Id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  Firstname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Lastname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Email: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Password: {
    type: Sequelize.STRING,
    allowNull: true
  },
  Birthdate: {
    type: Sequelize.DATE
  }
}, {
  modelName: 'User',
  sequelize,
  hooks: {
    beforeCreate : user => {
      const salt = bcrypt.genSaltSync();
      user.Password = bcrypt.hashSync(user.Password, salt);
    },
    beforeUpdate : user => {
      if(user.changed("Password")){
        const salt = bcrypt.genSaltSync();
        user.Password = bcrypt.hashSync(user.Password, salt);  
      }
    }
  },
});

User.prototype.toJSON =  function () {
  let user = Object.assign({}, this.get()) as User;
  delete user.Password;
  return user;
}
//#region Relations

// Relation 1 - 1 avec la liste de courses
User.hasOne(ShoppingList, {
  foreignKey: {
    allowNull: false
  }
});
ShoppingList.belongsTo(User);


User.hasMany(Recipe, {
  foreignKey: {
    allowNull: false
  }
});
Recipe.belongsTo(User);


//#endregion