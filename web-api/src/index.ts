import express from "express";
import routes from './routes/Routes';
import bodyParser from 'body-parser';
const history = require('connect-history-api-fallback');

const app = express();
const port = process.env.DB_port || 8080;

// Bodyparser - traite les requêtes.
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

// Setup server pour history mode Vue
app.use(express.static('public'));
app.use(history({
  disableDotRule: true,
  verbose: true,
  rewrites: [
      {
        from: /^\/api\/.*$/,
        to: function(context) {
            return context.parsedUrl.path
        }
      }
    ]
}));
app.use(express.static('public'));

app.listen(port, () => {
  return console.log(`[NodeJS] Server started at http://localhost:${port}`);
});

app.use("/api", routes);
