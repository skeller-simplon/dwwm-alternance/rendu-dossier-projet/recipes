import express from 'express'
const router = express()

import UserRouter from "./controllers/UserRoutes"
import RecipeRouter from "./controllers/RecipeRoutes"
import AuthRouter from "./controllers/AuthRoutes"
import IngredientRouter from './controllers/IngredientRoutes'
import RecipeIngredientRouter from './controllers/RecipeIngredientRoutes'
import ShoppingListController from './controllers/ShoppingListRoutes'


router.use('/users', UserRouter)
router.use('/recipes', RecipeRouter)
router.use('/ingredients', IngredientRouter)
router.use('/auth', AuthRouter)
router.use('/recipeIngredients', RecipeIngredientRouter)
router.use('/shoppingList', ShoppingListController)

export default router;
