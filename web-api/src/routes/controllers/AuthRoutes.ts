import express from "express";
import User from "../../db/models/User";
import * as authUtils from "../../utils/authUtils"
import FunctionalError from "../../transport/FunctionalError"
import bcrypt from "bcrypt";
import ControllerMiddleware from "../../utils/ControllerMiddleware";
const AuthRouter = express.Router();

AuthRouter.post('/login', ControllerMiddleware.checkBody(["email", "password"]), (req, res) => {
  let email = req.body.email;
  let password = req.body.password;
  try {
    User.findOne({ where: { Email: email } }).then(user => {
      if (!user)
        res.status(401).json(new FunctionalError("Votre email/votre mot de passe est invalide", 501))
      else {
        bcrypt.compare(password, user.Password).then(match => {
          if (!match)
            res.status(401).json(new FunctionalError("Votre email/votre mot de passe est invalide", 501))
          else {
            let token = authUtils.generateJWTToken({ email: email });
            res.json({
              success: true,
              message: 'Authentication successful!',
              token: token
            });
          }
        })
      }
    })
  } catch (error) {
    res.status(error.status || 400).json(error);
  }
});

AuthRouter.get("/getConnectedUser/", authUtils.checkToken, (req, res) => {
  try {
    res.json(req.body.user)
  } catch (err) {
    res.status(err.status || 400).json(err);
  }
})

export default AuthRouter;