import express from "express";
import Ingredient from "../../db/models/Ingredient";
import ControllerMiddleware from "../../utils/ControllerMiddleware"
import FunctionalError from "../../transport/FunctionalError";
const IngredientRouter = express.Router();

IngredientRouter.route("/").get(async (req, res) => {
    try {
        Ingredient.findAll().then(ingredients => {
            res.json(ingredients)
        })
    }catch(err) {
        res.status(err.status || 400).send(err)
    }
}).post(async (req, res) => {
    try {
         let ingredient = await Ingredient.findOne({where: {Label: req.body.Label}})
        if (!ingredient)
            Ingredient.create({Label: req.body.Label}).then(ingredient => res.status(200).send(ingredient))
        else
            throw new FunctionalError("Ingredient déjà existant")
    } catch (error) {
        console.log("YESSS")
        res.status(error.status || 400).send(error)
    }
}, ControllerMiddleware.checkBody(["Label"]))
IngredientRouter.route('/:id').get((req, res) => {
    const id: number = parseInt(req.params.id, 10);
    try {
        Ingredient.findOne({where: {Id: id}}).then(async ingredient => {
            res.json(ingredient)
        })
    } catch (err) {
        res.status(err.status || 400).send(err)
    }

    
})

IngredientRouter.route('/searchIngredients').post((req, res) => {
    try {
        const searchString = req.body.searchString
        const count = parseInt(req.body.count)
        Ingredient.findAll({
            where: {
                Label: {
                    $like: `%${searchString}%`
                }
            }, limit: count
        }).then(async ingredients => {
            res.json(ingredients)
        })
    } catch (err) {
        res.status(err.status || 400).send(err)
    }
}, ControllerMiddleware.checkBody(["searchString, count"]))

export default IngredientRouter; 