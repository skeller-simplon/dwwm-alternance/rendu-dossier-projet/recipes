import express from "express";
import RecipeIngredientType from "../../db/models/RecipeIngredientType"
const RecipeIngredientRouter = express.Router();

RecipeIngredientRouter.route("/types")
    .get(async (req, res) => {
        try {
            RecipeIngredientType.findAll().then(types => res.json(types));
        } catch (err) {
            res.status(err.status || 400).send(err)
        }
    })
export default RecipeIngredientRouter;