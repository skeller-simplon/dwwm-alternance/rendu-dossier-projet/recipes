import express from "express";
import User from "../../db/models/User";
import Recipe from "../../db/models/Recipe";
import RecipeIngredient from "../../db/models/RecipeIngredient";
import RecipeStep from "../../db/models/RecipeStep";
import Ingredient from "../../db/models/Ingredient";
import { Includeable, Order, Model, where, Sequelize } from "sequelize/types";
import FunctionalError from "../../transport/FunctionalError";
import { checkToken } from "../../utils/authUtils";
import ControllerMiddleware from "../../utils/ControllerMiddleware";
import RecipeIngredientType from "../../db/models/RecipeIngredientType";

const RecipeRouter = express.Router();
const FullRecipeIncludes: Includeable[] = [
    {model: RecipeIngredient, include: [{model: Ingredient}, {model: RecipeIngredientType}]},
    {model: RecipeStep},
    {model: User}
]

RecipeRouter.route('/searchRecipes').post((req, res) => {
    try {
        const searchString = req.body.searchString
        Recipe.findAll({
            where: {
                Title: {
                    $like: `%${searchString}%`
                }
            }
        }).then(recipes =>  res.json(recipes))
    } catch (err) {
        res.status(err.status || 400).send(err)
    }
}, ControllerMiddleware.checkBody(["searchString"]))

RecipeRouter.get("/byUserId/:userId", (req, res) => {
    const idUser: number = parseInt(req.params.userId, 10);
    try{
        Recipe.findAll({ where: {userId: idUser}})
        .then(recipes => res.json(recipes))
        .catch(err => {
            throw err;
        })
    }catch(err){
        res.status(err.status || 400).send(err)
    }
}, ControllerMiddleware.checkParams(["userId"]))

    RecipeRouter.get("/lastRecipes", async (req, res) => {
        try {
            const limit: number = req.query.limit ? parseInt(req.query.limit) : 10;
            let recipes = await Recipe.findAll({limit: limit, order: [['CreationDate', 'DESC']] })
            res.json(recipes)
        } catch (err) {
            res.status(err.status || 400).send(err)
        }
    })
RecipeRouter.route("/:id")
    .get(async (req, res) => {
        try {
            const id: number = parseInt(req.params.id, 10);
            const recipe = await Recipe.findOne({where:{ Id: id}, include: FullRecipeIncludes});
            res.send(recipe)
        } catch (err) {
            res.status(err.status || 400).send(err)
        }
    }, ControllerMiddleware.checkParams(["id"]))

RecipeRouter.post("/createWithFullDetail",
    checkToken, ControllerMiddleware.checkBody(["recipe"]),
    async (req, res) => {
    try {
        const recipe = req.body.recipe;
        const steps = req.body.steps;
        const ingredients = req.body.ingredients;
        const user = req.body.user;
        recipe.UserId = user.Id
        recipe.RecipeSteps = [];
        recipe.RecipeIngredients = [];
        Recipe.create(recipe).then(recip => {
                let stps = steps.forEach(recipeStep => {
                    recipeStep.RecipeId = recip.Id;
                    RecipeStep.create(recipeStep)
                })
                let ingrednts = ingredients.forEach(recipeIngredient => {
                    recipeIngredient.RecipeId = recip.Id;
                    recipeIngredient.IngredientId = recipeIngredient.Ingredient.Id
                    recipeIngredient.RecipeIngredientTypeId = recipeIngredient.RecipeIngredientType.Id
                    RecipeIngredient.create(recipeIngredient)
                });
                Promise.all([stps, ingrednts]).then((values) => {
                    res.json(recip)
                  });
        })
    } catch (err) {
        res.status(err.status || 400).send(err)
    }
})
export default RecipeRouter;