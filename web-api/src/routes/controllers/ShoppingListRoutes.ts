import express from "express";
import ControllerMiddleware from "../../utils/ControllerMiddleware";
import ShoppingList from "../../db/models/ShoppingList";
import Recipe from "../../db/models/Recipe";
import FunctionalError from "../../transport/FunctionalError";
import RecipeIngredient from "../../db/models/RecipeIngredient";
import Ingredient from "../../db/models/Ingredient";
import RecipeIngredientType from "../../db/models/RecipeIngredientType";
import { checkToken } from "../../utils/authUtils";

const ShoppingListRouter = express.Router();

ShoppingListRouter.route("/addRecipeToShoppingList")
    .post(ControllerMiddleware.checkBody(["userId", "recipeId"]), checkToken, (req, res) => {
        const recipeId: number = parseInt(req.body.recipeId, 10);
        const userId: number = parseInt(req.body.userId, 10);
        try {
            if (userId !== req.body.user.Id) throw new FunctionalError("Vous n'avez pas le droit de réaliser cette action")
            GetOrCreateShoppingListForUser(userId).then(list => {
                Recipe.findOne({where: {Id: recipeId}, include: [{
                    model: RecipeIngredient, include: [{model: Ingredient}, {model: RecipeIngredientType}]}]
                }).then(recipe => {
                    for (const ingredient of recipe.RecipeIngredients) {
                        RecipeIngredient.create({
                            Amount: ingredient.Amount,
                            IngredientId: ingredient.Ingredient.Id,
                            ShoppingListId: list.Id,
                            RecipeIngredientTypeId: ingredient.RecipeIngredientType.Id
                        })
                    }
                    res.send(list)
                }).catch(() => {
                    throw new FunctionalError("Impossible de retrouver la recette")
                })
            })
        } catch (error) {
            res.status(error.status || 400).send(error)
        }
    })
ShoppingListRouter.route("/deleteListForUser")
    .post(ControllerMiddleware.checkBody(["userId"]), checkToken, (req, res) => {
        const userId: number = parseInt(req.body.userId, 10);
        try {
            if (userId !== req.body.user.Id) throw new FunctionalError("Vous n'avez pas le droit de réaliser cette action")
            GetOrCreateShoppingListForUser(userId).then(list => {
                list.destroy();
                res.status(200).send();
            })
        } catch (error) {
            res.status(error.status || 400).send(error)
        }
    })
    ShoppingListRouter.route("/getShoppingListFromUser/")
    .get(ControllerMiddleware.checkQuery(["userId"]), checkToken, (req, res) => {
        const userId: number = parseInt(req.query.userId, 10);
        try {
            if (userId !== req.body.user.Id)
                throw new FunctionalError("Vous n'avez pas le droit de réaliser cette action")
            GetOrCreateShoppingListForUser(userId).then(list => res.send(list))
        } catch (error) {
            res.status(error.status || 400).send(error)
        }
    })
    
function GetOrCreateShoppingListForUser(userId: number){
    return ShoppingList.findOne({where: {UserId: userId}, include: [{
        model: RecipeIngredient, include: [{model: Ingredient}, {model: RecipeIngredientType}]}]
    }).then(list => {
        if(!list)
            return ShoppingList.create({UserId: userId, RecipeIngredients: []})
        else
            return list
    }).catch(() => { throw new FunctionalError("Impossible de retrouver la liste de course de l'utilisateur") })
}
export default ShoppingListRouter;
