import express from "express";
import User from "../../db/models/User";
import { FindOptions, where } from "sequelize/types";
import ControllerMiddleware from "../../utils/ControllerMiddleware"
import { checkEmail } from "../../utils/utils"
import FunctionalError from "../../transport/FunctionalError";

const UserRouter = express.Router();

UserRouter.route("/")
    .get(async (req, res) => {
        try {
            let users = await User.findAll({})
            res.json(users)
        } catch (err) {
            res.status(err.status || 400).send(err)
        }
    })
    .post(ControllerMiddleware.checkBody(["Firstname", "Lastname", "Email", "Password", "Birthdate"]), async (req, res) => {
        try {
            let user = new User();
            await checkEmail(req.body.Email);
            user.Firstname = req.body.Firstname;
            user.Lastname = req.body.Lastname;
            user.Birthdate = req.body.Birthdate;
            user.Email = req.body.Email;
            user.Password = req.body.Password
            user.save();
            res.status(200).send(user)
        } catch (error) {
            res.status(500).json(error)
        }
    })

UserRouter.route('/searchUsers').post((req, res) => {
    try {
        const searchString = req.body.searchString
        User.findAll({
            where: {
                Firstname: { $like: `%${searchString}%` },
                Lastname: { $like: `%${searchString}%` }
            }
        }).then(ingredients => res.json(ingredients) )
    } catch (err) {
        res.status(err.status || 400).send(err)
    }
}, ControllerMiddleware.checkBody(["searchString"]))

UserRouter.route("/:id")
    .get(async (req, res) => {
        const id: number = parseInt(req.params.id, 10);
        try {
            let user = await User.findByPk(id);
            if (user){
                res.send(user)
            }else {
                throw new FunctionalError("Impossible de retrouver l'utilisateur")
            }
        } catch (error) {
            res.status(error.status || 401).json(error)
        }
    })
    .put(async (req, res) => {
        const id: number = parseInt(req.params.id, 10);
        try {
            let user = await User.findByPk(id);
            if (!user)
                throw new Error("Impossible de retrouver l'utilisateur")

            if (req.body.Firstname)
                user.Firstname = req.body.Firstname;
            if (req.body.Lastname)
                user.Lastname = req.body.Lastname;
            if (req.body.Birthdate)
                user.Birthdate = req.body.Birthdate;
            if (req.body.Email) {
                checkEmail(req.body.Email)
                user.Email = req.body.Email;
            }
            if (req.body.Password)
                user.Password = req.body.Password

            user.save();
            res.send(user)
        } catch (error) {
            res.status(error.status || 400).send(error)
        }
    })
export default UserRouter;
