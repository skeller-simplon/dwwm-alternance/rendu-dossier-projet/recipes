'use strict';

var util = require('util');

function ExpiredTokenError(message: string, code: number = 400) {
  Error.captureStackTrace(this, ExpiredTokenError);
  this.name = ExpiredTokenError.name;
  this.code = code;
  this.message = message;
}

util.inherits(ExpiredTokenError, Error);

export default ExpiredTokenError;
