'use strict';

var util = require('util');

function FunctionalError(message: string, code: number = 400) {
  Error.captureStackTrace(this, FunctionalError);
  this.name = FunctionalError.name;
  this.code = code;
  this.message = message;
}

util.inherits(FunctionalError, Error);

export default FunctionalError;