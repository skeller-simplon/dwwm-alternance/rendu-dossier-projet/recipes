import FunctionalError from "../transport/FunctionalError";

export default class ControllerMiddleware {

    static checkBody = (arr: string[]) => ControllerMiddleware.checkParamsWithSource(arr, "body")
    static checkParams = (arr: string[]) => ControllerMiddleware.checkParamsWithSource(arr, "params")
    static checkQuery = (arr: string[]) => ControllerMiddleware.checkParamsWithSource(arr, "query")

    private static checkParamsWithSource(arr: string[], source: string) {
        return function (req, res, next) {
            let missing_params = [];
            for (var i = 0; i < arr.length; i++) {
                if (!eval( `req.${source}.${arr[i]}`)) {
                    missing_params.push(arr[i]);
                }
            }
            if (missing_params.length == 0) {
                next();
            } else {
            throw new FunctionalError("Parametre(s) manquant: " + missing_params.join(","), 402)
            }
        }
    }
}