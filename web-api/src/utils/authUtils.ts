import jwt from 'jsonwebtoken';
import User from '../db/models/User';
import FunctionalError from '../transport/FunctionalError';

const SECRET_KEY = "SECRET_INGREDIENT";
const jwtOptions = { 
   expiresIn: '6h' // expires in 24 hours
}

export interface JwtDataType {
   email: string
}
export const generateJWTToken = (userData) =>{
    return jwt.sign(userData, SECRET_KEY, jwtOptions);
 }
 export const verifyToken = (jwtToken): JwtDataType | null =>{
    try{
       return jwt.verify(jwtToken, SECRET_KEY) as JwtDataType;
    }catch(e){
       if (e.name === "TokenExpiredError")
         e.message = "Votre session à expirée, veuillez vous reconnecter.";
       throw e;
    }
 }

 export const checkToken = (req, res, next) => {
    try {
      const token = req.headers.authorization.toString().split(' ')[1];
      if(typeof token !== 'undefined') {
          let validated = verifyToken(token)
          User.findOne({where: {Email: validated.email}}).then(user => {
             if(user){
               req.body.user = user
               next();
             }
             else throw new FunctionalError("Token invalide", 403)
          })
      } else {
          throw new FunctionalError("Vous n'avez pas l'autorisation de réaliser cette action", 403)
      }
   }
   catch (err) {
      res.status(err.status || 400).send(err)
   }
}