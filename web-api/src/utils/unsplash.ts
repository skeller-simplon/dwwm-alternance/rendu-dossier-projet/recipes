// ES Modules syntax
import Unsplash, {toJson} from 'unsplash-js';

import fetch from 'node-fetch';
// import FunctionalError from '../transport/FunctionalError';

const globalAny:any = global;
(global as any).fetch = fetch;

const accessKey = process.env.UNSPLASH_ACCESS_KEY

const unsplash = new Unsplash({ accessKey: accessKey });

const noPhotoFound = 'https://i.ibb.co/GVN1TZF/unnamed.jpg';

const computeImage = textToFindImage => 
  unsplash.search.photos(textToFindImage, 1)
  .then(toJson)
  .then(async json => {
    if (!json.total)
      return noPhotoFound;
    return json["results"][0].urls.small;
  }).catch(err => {
    throw err
  });

export default computeImage