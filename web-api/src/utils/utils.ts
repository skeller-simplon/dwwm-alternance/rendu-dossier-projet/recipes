import validator from "validator"
import User from "../db/models/User";
import FunctionalError from "../transport/FunctionalError";

export const checkEmail = async (email: string) => {
    if (!email)
        return;
    if (!validator.isEmail(email))
        throw new FunctionalError("Email non valide", 401)
    let oldUser = await User.findOne({where: {Email: email}})
    if (oldUser)
        throw new FunctionalError("Email déjà utilisé", 500);
}